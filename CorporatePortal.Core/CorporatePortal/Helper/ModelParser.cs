﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporatePortal.Helper
{
    public static class ModelParser
    {
        /// <summary>
        /// Преобразование Сущности в Модель Задачи 
        /// </summary>
        /// <param name="entity"> Сущность </param>
        /// <returns> Модель </returns>
        public static Models.TaskList ToDto(this Entities.TaskList entity)
        {
            return new Models.TaskList
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                Deadline = entity.Deadline,
                IsDone = entity.IsDone
            };
        }

        /// <summary>
        /// Преобразование Модели в Сущность Задачи
        /// </summary>
        /// <param name="entity"> Модель </param>
        /// <returns> Сущность </returns>
        public static Entities.TaskList ToEntity(this Models.TaskList dto)
        {
            return new Entities.TaskList
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description,
                Deadline = dto.Deadline,
                IsDone = dto.IsDone
            };
        }

        /// <summary>
        /// Преобразование Сущности в Модель Оценка
        /// </summary>
        /// <param name="entity"> Сущность </param>
        /// <returns> Модель </returns>
        public static Models.MarkList ToDto(this Entities.MarkList entity)
        {
            return new Models.MarkList
            {
                Id = entity.Id,
                Name = entity.Name,
                Error = entity.Error,
                Time = entity.Time
            };
        }

        /// <summary>
        /// Преобразование Модели в Сущность Оценка
        /// </summary>
        /// <param name="entity"> Модель </param>
        /// <returns> Сущность </returns>
        public static Entities.MarkList ToEntity(this Models.MarkList dto)
        {
            return new Entities.MarkList
            {
                Id = dto.Id,
                Name = dto.Name,
                Error = dto.Error,
                Time = dto.Time
            };
        }
    }
}
