#pragma checksum "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Chat\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2533823c9e1e8e4752792ff6773feee75cb0f30a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Chat_Index), @"mvc.1.0.view", @"/Views/Chat/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\_ViewImports.cshtml"
using CorporatePortal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\_ViewImports.cshtml"
using CorporatePortal.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2533823c9e1e8e4752792ff6773feee75cb0f30a", @"/Views/Chat/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6b1ba8687e65e0e44367042db02d060d366bd472", @"/Views/_ViewImports.cshtml")]
    public class Views_Chat_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Chat\Index.cshtml"
  
    Layout = null;

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 5 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Chat\Index.cshtml"
 using (Html.BeginForm("Index", "Chat", FormMethod.Post, new { onsubmit = "return SubmitForm(this)" }))
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <footer class=\"border-top footer\">\r\n    <div>\r\n        <a class=\"btn btn-info\" style=\"margin-bottom:5px\"");
            BeginWriteAttribute("onclick", " onclick=\"", 245, "\"", 298, 3);
            WriteAttributeValue("", 255, "PopupForm(\'", 255, 11, true);
#nullable restore
#line 9 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Chat\Index.cshtml"
WriteAttributeValue("", 266, Url.Action("Graduate","Chat"), 266, 30, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 296, "\')", 296, 2, true);
            EndWriteAttribute();
            WriteLiteral("> Отпуск</a>\r\n        <a class=\"btn btn-info\" style=\"margin-bottom:5px\"");
            BeginWriteAttribute("onclick", " onclick=\"", 370, "\"", 421, 3);
            WriteAttributeValue("", 380, "PopupForm(\'", 380, 11, true);
#nullable restore
#line 10 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Chat\Index.cshtml"
WriteAttributeValue("", 391, Url.Action("Money", "Chat"), 391, 28, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 419, "\')", 419, 2, true);
            EndWriteAttribute();
            WriteLiteral("> ЗП</a>\r\n    </div>\r\n    </footer>\r\n");
#nullable restore
#line 13 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Chat\Index.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
        <script>
            //Форма открывается при нажатии кнопки ""Добавить"" или ""Изменить""
            function PopupForm(url) {
                var formDiv = $('<div/>');
                $.get(url)
                    .done(function (response) {
                        formDiv.html(response);

                        Popup = formDiv.dialog({
                            autoOpen: true,
                            resizable: false,
                            title: 'Бот',
                            height: 400,
                            width: 300,
                            close: function () {
                                Popup.dialog('destroy').remove();
                            }
                        });
                    });
            }
        </script>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
