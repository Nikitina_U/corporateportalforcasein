#pragma checksum "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dd8eea412ace4f7555352bfea0a02a885f1b28e6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Task_Edit), @"mvc.1.0.view", @"/Views/Task/Edit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\_ViewImports.cshtml"
using CorporatePortal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\_ViewImports.cshtml"
using CorporatePortal.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dd8eea412ace4f7555352bfea0a02a885f1b28e6", @"/Views/Task/Edit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6b1ba8687e65e0e44367042db02d060d366bd472", @"/Views/_ViewImports.cshtml")]
    public class Views_Task_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CorporatePortal.Models.TaskList>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-check-input"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
  
    Layout = null;

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 7 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
 using (Html.BeginForm("Edit", "Task", FormMethod.Post, new { onsubmit = "return SubmitForm(this)" }))
{
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
Write(Html.HiddenFor(model => model.Id));

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div class\"form-group\">\r\n        ");
#nullable restore
#line 11 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
   Write(Html.LabelFor(model => model.Name, new { @class = "control-ladel" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 12 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
   Write(Html.ValueFor(model => model.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        \r\n    </div>\r\n    <div class\"form-group\">\r\n        ");
#nullable restore
#line 16 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
   Write(Html.LabelFor(model => model.Description, new { @class = "control-ladel" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 17 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
   Write(Html.ValueFor(model => model.Description));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class\"form-group\">\r\n        ");
#nullable restore
#line 20 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
   Write(Html.LabelFor(model => model.Deadline, new { @class = "control-ladel" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        ");
#nullable restore
#line 21 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
   Write(Html.ValueFor(model => model.Deadline));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n    <div class=\"form-group form-check\">\r\n        <label class=\"form-check-label\">\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "dd8eea412ace4f7555352bfea0a02a885f1b28e66749", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
#nullable restore
#line 25 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.IsDone);

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(" ");
#nullable restore
#line 25 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
                                                           Write(Html.DisplayNameFor(model => model.IsDone));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
        </label>
    </div>
    <div class=""form-group"">
        <input type=""submit"" style=""margin-top: 10px"" value=""Сохранить"" class=""btn btn-primary"" />
        <input type=""reset"" style=""margin-top: 10px"" value=""Сбросить"" class=""btn"" />
    </div>
");
#nullable restore
#line 32 "C:\Users\Uliana\corporateportalforcasein\CorporatePortal.Core\CorporatePortal\Views\Task\Edit.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("<div>\r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "dd8eea412ace4f7555352bfea0a02a885f1b28e69111", async() => {
                WriteLiteral("Вернуться назад");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</div>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CorporatePortal.Models.TaskList> Html { get; private set; }
    }
}
#pragma warning restore 1591
