﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporatePortal.Controllers
{
    /// <summary>
    /// Контроллер для работы с диаграммой
    /// </summary>
    public class ChartController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
