﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporatePortal.Controllers
{
    /// <summary>
    /// Контроллер для работы с нормативными документами
    /// </summary>
    public class NormativeDocumentsController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
