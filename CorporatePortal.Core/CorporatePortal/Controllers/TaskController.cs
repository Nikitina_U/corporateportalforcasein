﻿using CorporatePortal.Data;
using CorporatePortal.Helper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskDb = CorporatePortal.Entities.TaskList;
using TaskDto = CorporatePortal.Models.TaskList;

namespace CorporatePortal.Controllers
{
    /// <summary>
    /// Контроллер для работы с задачами (только для новичка)
    /// </summary>
    [Route("Task")]
    public class TaskController : Controller
    {
        private readonly ApplicationDbContext _appDBContext;

        public TaskController(ApplicationDbContext appDBContext)
        {
            _appDBContext = appDBContext;
        }
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Получение списка всех задач из БД
        /// </summary>
        [Route("GetData")]
        public ActionResult GetData()
        {
            List<TaskDto> taskList = _appDBContext.TaskList.ToList().Select(x => x.ToDto()).ToList();

            return Json(new { data = taskList });
        }

        [Route("Edit/{id}")]
        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            var record = _appDBContext.TaskList.Where(x => x.Id == id).FirstOrDefault().ToDto();
            return View(record);

        }

        /// <summary>
        /// Ре5дактирвоание задачи
        /// </summary>
        /// <param name="task">Задача</param>
        [Route("Edit/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(TaskDto task)
        {
                var record = _appDBContext.TaskList.Where(x => x.Id == task.Id).FirstOrDefault();              
                record.IsDone = task.IsDone;
                _appDBContext.TaskList.Update(record);
                await _appDBContext.SaveChangesAsync();
            return Json(new { success = true, message = "Запись изменена успешно" });
        }
    }
}
