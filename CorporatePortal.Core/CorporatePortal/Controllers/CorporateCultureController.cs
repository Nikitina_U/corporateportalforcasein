﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporatePortal.Controllers
{
    /// <summary>
    /// Контроллер для работы с корпоративной культурой
    /// </summary>
    public class CorporateCultureController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
