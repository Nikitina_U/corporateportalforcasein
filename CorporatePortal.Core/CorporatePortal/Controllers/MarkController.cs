﻿using CorporatePortal.Data;
using CorporatePortal.Helper;
using CorporatePortal.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarkDb = CorporatePortal.Entities.MarkList;
using MarkDto = CorporatePortal.Models.MarkList;

namespace CorporatePortal.Controllers
{
    /// <summary>
    /// Контроллер для работы с выставлением оценок при прохождении испытаний
    /// </summary>
    [Route("Mark")]
    public class MarkController : Controller
    {
        private readonly ApplicationDbContext _appDBContext;

        public MarkController(ApplicationDbContext appDBContext)
        {
            _appDBContext = appDBContext;
        }

        [Route("Index")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("MarkList")]
        public ActionResult MarkList()
        {
            return View();
        }

        [Route("GetData")]
        public ActionResult GetData()
        {
            List<MarkDto> taskList = _appDBContext.MarkList.ToList().Select(x => x.ToDto()).ToList();

            return Json(new { data = taskList });
        }


        [Route("Add")]
        [HttpGet]
        public ActionResult Add(int id = 0)
        {
            return View(new MarkList());
        }

        [Route("Edit/{id}")]
        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            var record = _appDBContext.MarkList.Where(x => x.Id == id).FirstOrDefault().ToDto();
            return View(record);

        }

        [Route("Add")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(MarkDto task)
        {
            if (ModelState.IsValid)
            {
                _appDBContext.MarkList.Add(task.ToEntity());
                await _appDBContext.SaveChangesAsync();
            }

            return Json(new { success = true, message = "Запись сохранена успешно" });
        }

        [Route("Edit/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(MarkDto task)
        {
            if (ModelState.IsValid)
            {
                var record = _appDBContext.MarkList.Where(x => x.Id == task.Id).FirstOrDefault();
                record.Name = task.Name;
                record.Error = task.Error;
                record.Time = task.Time;
                _appDBContext.MarkList.Update(record);
                await _appDBContext.SaveChangesAsync();
            }
            return Json(new { success = true, message = "Запись изменена успешно" });
        }

        [Route("Delete/{id}")]
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var record = _appDBContext.MarkList.Where(x => x.Id == id).FirstOrDefault();
            _appDBContext.MarkList.Remove(record);
            await _appDBContext.SaveChangesAsync();
            return Json(new { success = true, message = "Запись удалена успешно" });
        }
    }
}
