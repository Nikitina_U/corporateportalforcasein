﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CorporatePortal.Entities
{
    /// <summary>
    /// Класс сущности оценки
    /// </summary>
    public class MarkList
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Тип проверки 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ошибки
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Время выполнения
        /// </summary>
        public string Time { get; set; }
    }
}
