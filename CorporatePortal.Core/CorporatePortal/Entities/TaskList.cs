﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CorporatePortal.Entities
{
    /// <summary>
    /// Класс сущности задачи
    /// </summary>
    public class TaskList
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Срок
        /// </summary>
        public DateTime Deadline { get; set; }

        /// <summary>
        /// Выполнил
        /// </summary>
        public bool IsDone { get; set; }
    }
}
