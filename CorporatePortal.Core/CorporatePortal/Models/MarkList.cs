﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CorporatePortal.Models
{
    /// <summary>
    /// Модель Оценки
    /// </summary>
    public class MarkList
    {
        [JsonPropertyName("Id")]
        public int Id { get; set; }

        [Display(Name = "Тип проверки ")]
        [JsonPropertyName("Name")]
        public string Name { get; set; }

        [Display(Name = "Ошибки")]
        [JsonPropertyName("Error")]
        public string Error { get; set; }

        [Display(Name = "Время")]
        [JsonPropertyName("Time")]
        public string Time { get; set; }
    }
}
