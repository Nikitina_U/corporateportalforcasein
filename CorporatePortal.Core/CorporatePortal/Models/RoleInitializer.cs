﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorporatePortal.Models
{
    /// <summary>
    /// Класс для инициализации ролей и логин/пароль по умолчанию для этих ролей. 
    /// </summary>
    public class RoleInitializer
    {
        public static async Task InitializeAsync(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            //Роль Админа (Куратора)
            string adminEmail = "admin@gmail.com";
            string password = "_Aa123456";
            
            //Роль Новичка (Сотрудника)
            string employeeEmail = "employee@gmail.com";
            string employeePassword = "_Aa123456";

            //Роль Оператор
            string operatorEmail = "operator@gmail.com";
            string operatorPassword = "_Aa123456";

            if (await roleManager.FindByNameAsync("admin") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("admin"));
            }
            if (await roleManager.FindByNameAsync("employee") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("employee"));
            }
            if (await roleManager.FindByNameAsync("operator") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("operator"));
            }
            if (await userManager.FindByNameAsync(adminEmail) == null)
            {
                var admin = new IdentityUser(adminEmail);
                IdentityResult result = await userManager.CreateAsync(admin, password);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "admin"); //Куратор
                }
            }
            if (await userManager.FindByNameAsync(employeeEmail) == null)
            {
                var employee = new IdentityUser(employeeEmail);
                IdentityResult result = await userManager.CreateAsync(employee, employeePassword);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(employee, "employee"); //Сотрудник
                }
            }
            if (await userManager.FindByNameAsync(operatorEmail) == null)
            {
                var operatoR = new IdentityUser(operatorEmail);
                IdentityResult result = await userManager.CreateAsync(operatoR, operatorPassword);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(operatoR, "operator"); //Сотрудник
                }
            }
        }
    }
}
